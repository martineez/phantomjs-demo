package com.example.tests;

import java.io.File;
import java.security.Key;
import java.util.concurrent.TimeUnit;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.CoreMatchers.containsString;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.phantomjs.PhantomJSDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class SampleHeadlessTest {
    private WebDriver driver;
    private String baseUrl;

    @BeforeClass
    public void setUp() throws Exception {
        DesiredCapabilities dc = new DesiredCapabilities();
        dc.setJavascriptEnabled(true);
        dc.setCapability("takesScreenshot", false);

        driver = new PhantomJSDriver(dc);

        // unzip http://chromedriver.storage.googleapis.com/2.21/chromedriver_<platform>.zip to project folder
        // or set property webdriver.chrome.driver:
        // System.setProperty("webdriver.chrome.driver", "target/chromedriver");
        // driver = new ChromeDriver(dc);

        // driver = new FirefoxDriver(dc);


        baseUrl = "https://github.com";
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
    }

    @Test
    public void filterReturnsRepository() throws Exception {
        driver.get(baseUrl + "/vtajzich?tab=repositories");
        WebElement filter = driver.findElement(By.id("your-repos-filter"));

        //filter.sendKeys("phantomjs");

        WebElement results = driver.findElement(By.className("repo-list"));
        assertThat("repo not found", results.getText(), containsString("Docker-PhantomJS"));
    }

    @AfterClass
    public void tearDown() throws Exception {
        File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
        FileUtils.copyFile(scrFile, new File("github.png"));

        driver.quit();
    }
}

